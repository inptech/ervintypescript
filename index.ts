interface UserInterface {
        name: string;
        age?: number;
        getMessage(): string;
    }
    
    const user: UserInterface = {
        name: "Ahmed",
        age: 18,
        getMessage() {
            return "Hello" + name;
        },
    };
    
    const user2: UserInterface = {
        name: "Ervin",
        getMessage() {
            return "Hello" + name;
        },
     };

     console.log(user.getMessage());